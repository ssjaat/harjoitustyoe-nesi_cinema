alleviiv = '\033[4m'
norm = '\033[0m'
print(alleviiv + "Varaukset ja elokuvien ohjelmisto"+ norm)
print()
import json   
tiedosto = open("NeSi_naytokset.json", "r")
sisalto = tiedosto.read()
näytökset = json.loads(sisalto)

for näytös in näytökset:
    print(f"{näytös}|{näytökset[näytös][0]}  ->{näytökset[näytös][1]}{näytökset[näytös][2]}")
    näytöslista = näytökset[näytös]
   
    varatut_paikat = näytöslista[3]
    
    if näytöslista[2] == "sali 1":
        paikka = []
        for xd in range(15):
            rivi = []
            xda = 0
            for xda in range(15):
                rivi.append(" O")
            paikka.append(rivi)
        for x in varatut_paikat:
            sn = x.split(",")
            paikka[(int(sn[0]) - 1)][(int(sn[1]))] = " X"
        dx = 1
        for rivi in paikka:
            if len(str(dx)) < 2:
                de = " " + str(dx)
            else:
                de = dx
            print ("Rivi: " + str(de) + " ".join(rivi))
            dx = dx + 1
                
    if näytöslista[2] == "sali 2":
        
        paikka = []
        for xd in range(3):
            rivi = []
            xda = 0
            for xda in range(5):
                rivi.append(" *")
            paikka.append(rivi)
        for x in varatut_paikat:
            sn = x.split(",")
            paikka[(int(sn[0]) - 1)][(int(sn[1]))] = " X"
        dx = 1
        for rivi in paikka:
            if len(str(dx)) < 2:
                de = " " + str(dx)
            else:
                de = dx
            print ("Rivi: " + str(de) + " ".join(rivi))
            dx = dx + 1
    if näytöslista[2] == "sali 3":
        paikka = []
        for xd in range(6):
            rivi = []
            xda = 0
            for xda in range(6):
                rivi.append(" []")
            paikka.append(rivi)
        for x in varatut_paikat:
            sn = x.split(",")
            paikka[(int(sn[0]) - 1)][(int(sn[1]))] = "  X"
        dx = 1
        for rivi in paikka:
            if len(str(dx)) < 2:
                de = " " + str(dx)
            else:
                de = dx
            print ("Rivi: " + str(de) + " ".join(rivi))
            dx = dx + 1      
print()
print("LISÄÄ OHJELMISTOON:")
print()
nimi = input("Uuden elokuvan nimi: ")
pvm = input("Päivämäärä ja aika (muodossa: 2022-01-01  00:00): ")
sali = input("Salin numero (muoto: sali x): ")
laskuri = 1
while str(laskuri) in näytökset:
    laskuri += 1
näytökset[str(laskuri)] = [nimi, pvm, sali,[]]
tiedosto = open("NeSi_naytokset.json", "w")
json.dump(näytökset, tiedosto, indent=2)
tiedosto.close()
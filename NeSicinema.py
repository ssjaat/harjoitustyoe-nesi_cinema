sin = '\033[34m'
pink  = '\033[95m'
pun = '\033[31m'
vihr = '\033[32m'
alleviiv = '\033[4m'
norm = '\033[0m'

print(sin + alleviiv + "NeSi cinema")
print()
print(norm + "Tervetuloa nauttimaan ikimuistoisista hetkistä elokuvien parissa! Omat eväät ovat meidän elokuvateatterissa sallittuja!")
print()

print()

import json   
tiedosto = open("NeSi_naytokset.json", "r")
sisalto = tiedosto.read()
näytökset = json.loads(sisalto)



print()
print(sin + "Sali 1" + norm, "(NORMAALI)")
print(sin + "Sali 2" + norm, "(LUXE)")
print(sin + "Sali 3" + norm, "(LOUNGE + iSENSE)")
print()
        

def valitse_näytös():
    print(norm + alleviiv + "Ohjelmistossa nyt:")
    print()
    for näytös in näytökset:
        print(norm + pink + f"{näytös}|{näytökset[näytös][0]}"  + norm, f"-> {näytökset[näytös][1]} | {näytökset[näytös][2]}")
    print()
    valinta = input(norm + "Mihin näytökseen tahtoisit ostaa liput? (anna numero): ")
    print()

    return näytökset[valinta]
    
    
print()

salien_paikkamaara = {"Sali 1": 225, "Sali 2": 15, "Sali 3": 9}


näytöslista = valitse_näytös()
if näytöslista[2] == "sali 1":
    print("Salissa on 225 paikkaa")
    print("Valitse vapaa paikka salista")
    print()
    def nayta_paikat(varatut_paikat):
        paikka = []
        for xd in range(15):
            rivi = []
            xda = 0
            for xda in range(15):
                rivi.append(" O")
            paikka.append(rivi)
        for x in varatut_paikat:
            sn = x.split(",")
            paikka[(int(sn[0]) - 1)][(int(sn[1]))] = " X"
        dx = 1
        for rivi in paikka:
            if len(str(dx)) < 2:
                de = " " + str(dx)
            else:
                de = dx
            print ("Rivi: " + str(de) + " ".join(rivi))
            dx = dx + 1
    def vaihtoehdot():
        print ("1: Näytä salikartta")
        print ("2: Näytä hinta riville")
        print ("3: Hinta tällä hetkellä")
        print ("4: Lippujen ostaminen")
        print ("5: Poistu valinnasta")
        uusi_input = input("Valintasi: ")
        return (uusi_input)
    def ostetut_paikat(varatut_paikat):
        kartta = input("Näytetäänkö salikartta k/e?: ")
        if kartta == "k":
            nayta_paikat(varatut_paikat)
        x = True
        while x == True:
            hinta = 0
            rivix = int(input("Miltä riviltä haluat paikan? "))
            riviy = int(input("Minkä paikan riviltä haluat ostaa? "))
            if any((str(rivix) + "," + str(riviy)) in varatut_paikat for x in varatut_paikat):
                print (pun + "Paikka on jo varattu, valitse toinen paikka" + norm)
            elif int(rivix) > 15 or int(riviy) > 15:
                print ("Virheellinen paikka valinta, valitse toinen paikka")
            else:
                print (vihr + "Paikka varattu!" + norm)
                hinta = (2 * int(rivix))
                x = False
        return (hinta, (str(rivix) + "," + str(riviy)))
    varatut_paikat = näytöslista[3]
    myynti = 0
    ns = 0
    while ns == 0:
        uusi_input = vaihtoehdot()
        if uusi_input == "5":
            ns = 1
        elif uusi_input == "4":
            g = True
            while g == True:
                uusi_paikka = ostetut_paikat(varatut_paikat)
                varatut_paikat.append(uusi_paikka[1])
                print ("Se tekee: " + str(uusi_paikka[0]) + " €")
                myynti = myynti + uusi_paikka[0]
                print ("Haluatko ostaa toisen paikan?")
                uusi_input = input("1 = kyllä, 2 = ei: ")
                if uusi_input == "1":
                    pass
                else:
                    g = False
        elif uusi_input == "3":
            print ("Kokonaishinta:", str(myynti) + "€")
        elif uusi_input == "2":
            xd = 0
            while xd < 15:
                print ("Rivi " + str(xd + 1) + ": on " + str((1 + (2 * xd)) + 1) + " €" )
                xd = xd + 1
        elif uusi_input == "1":
            nayta_paikat(varatut_paikat)
        else:
            print (pun + "Virheellinen vaihtoehto" + norm)
            
if näytöslista[2] == "sali 2":
    print("Sali on LUXE-sali, ja siellä on 15 paikkaa")
    print("Huomaathan, että LUXE-sali on kalliimpi normaaliin saliin verrattuna!")
    print()
    print("Valitse vapaa paikka salista")
    def nayta_paikat(varatut_paikat):
        paikka = []
        for xd in range(3):
            rivi = []
            xda = 0
            for xda in range(5):
                rivi.append(" *")
            paikka.append(rivi)
        for x in varatut_paikat:
            sn = x.split(",")
            paikka[(int(sn[0]) - 1)][(int(sn[1]))] = " X"
        dx = 1
        for rivi in paikka:
            if len(str(dx)) < 2:
                de = " " + str(dx)
            else:
                de = dx
            print ("Rivi: " + str(de) + " ".join(rivi))
            dx = dx + 1
    def vaihtoehdot():
        print ("1: Näytä salikartta")
        print ("2: Näytä hinta riville")
        print ("3: Hinta tällä hetkellä")
        print ("4: Lippujen ostaminen")
        print ("5: Poistu valinnasta")
        uusi_input = input("Valintasi: ")
        return (uusi_input)
    def ostetut_paikat(varatut_paikat):
        kartta = input("Näytetäänkö salikartta k/e?: ")
        if kartta == "k":
            nayta_paikat(varatut_paikat)
        x = True
        while x == True:
            hinta = 0
            rivix = int(input("Miltä riviltä haluat paikan? "))
            riviy = int(input("Minkä paikan riviltä haluat ostaa? "))
            if any((str(rivix) + "," + str(riviy)) in varatut_paikat for X in varatut_paikat):
                print (pun + "Paikka on jo varattu, valitse toinen paikka" + norm)
            elif int(rivix) > 5 or int(riviy) > 3:
                print (pun + "Virheellinen paikka valinta, valitse toinen paikka" + norm)
            else:
                print (vihr + "Paikka varattu!" + norm)
                hinta = (4.5 * int(rivix) * 5)
                x = False
        return (hinta, (str(rivix) + "," + str(riviy)))
    varatut_paikat = näytöslista[3]
    myynti = 0
    ns = 0
    while ns == 0:
        uusi_input = vaihtoehdot()
        if uusi_input == "5":
            ns = 1
        elif uusi_input == "4":
            g = True
            while g == True:
                uusi_paikka = ostetut_paikat(varatut_paikat)
                varatut_paikat.append(uusi_paikka[1])
                print ("Se tekee: " + str(uusi_paikka[0]) + " €")
                myynti = myynti + uusi_paikka[0]
                print ("Haluatko ostaa toisen paikan?")
                uusi_input = input("1 = kyllä, 2 = ei: ")
                if uusi_input == "1":
                    pass
                else:
                    g = False
        elif uusi_input == "3":
            print ("Kokonaishinta:", str(myynti) + "€")
        elif uusi_input == "2":
            xd = 0
            while xd < 3:
                print ("Rivi " + str(xd + 1) + ": on " + str((1 + (4.5 * xd)) + 25) + " €" )
                xd = xd + 1
        elif uusi_input == "1":
            nayta_paikat(varatut_paikat)
        else:
            print (pun + "Virheellinen vaihtoehto" + norm)
    print()        
    print("Muista, että LUXE-sali on", pun+ "K-18" +norm ,"eli varaudu esittämään voimassaoleva henkilöllisyystodistus!")
    print()
    print(alleviiv + "Omat alkoholijuomat" + norm, "ovat salissamme sallittuja!" )
            

if näytöslista[2] == "sali 3":
    print("Sali on LOUNGE + iSENSE-sali, jossa on ääniefektien lisäksi muita elkouva kokemusta parantavia efektejä, ole siis valmis kaikkeen! LOUNGE-salille tyypilliset mukavat nojatuolit tyynyineen ja peittoineen takaavat ikimuistoisen elokuva hetken!")
    print("Huomaathan, että LOUNGE + iSENSE-sali on kalliimpi normaaliin saliin verrattuna!")
    print()
    print("Valitse vapaa paikka salista")
    def nayta_paikat(varatut_paikat):
        paikka = []
        for xd in range(6):
            rivi = []
            xda = 0
            for xda in range(6):
                rivi.append(" []")
            paikka.append(rivi)
        for x in varatut_paikat:
            sn = x.split(",")
            paikka[(int(sn[0]) - 1)][(int(sn[1]))] = "  X"
        dx = 1
        for rivi in paikka:
            if len(str(dx)) < 2:
                de = " " + str(dx)
            else:
                de = dx
            print ("Rivi: " + str(de) + " ".join(rivi))
            dx = dx + 1
    def vaihtoehdot():
        print ("1: Näytä salikartta")
        print ("2: Näytä hinta riville")
        print ("3: Hinta tällä hetkellä")
        print ("4: Lippujen ostaminen")
        print ("5: Poistu valinnasta")
        uusi_input = input("Valintasi: ")
        return (uusi_input)
    def ostetut_paikat(varatut_paikat):
        kartta = input("Näytetäänkö salikartta k/e?: ")
        if kartta == "k":
            nayta_paikat(varatut_paikat)
        x = True
        while x == True:
            hinta = 0
            rivix = int(input("Miltä riviltä haluat paikan? "))
            riviy = int(input("Minkä paikan riviltä haluat ostaa? "))
            if any((str(rivix) + "," + str(riviy)) in varatut_paikat for X in varatut_paikat):
                print ("Paikka on jo varattu, valitse toinen paikka")
            elif int(rivix) > 6 or int(riviy) > 6:
                print (pun + "Virheellinen paikka valinta, valitse toinen paikka" + norm)
            else:
                print (vihr + "Paikka varattu!" + norm)
                hinta = (20 * int(rivix))
                x = False
        return (hinta, (str(rivix) + "," + str(riviy)))
    varatut_paikat = näytöslista[3]
    myynti = 0
    ns = 0
    while ns == 0:
        uusi_input = vaihtoehdot()
        if uusi_input == "5":
            ns = 1
        elif uusi_input == "4":
            g = True
            while g == True:
                uusi_paikka = ostetut_paikat(varatut_paikat)
                varatut_paikat.append(uusi_paikka[1])
                print ("Se tekee: " + str(uusi_paikka[0])+ "€")
                myynti = myynti + uusi_paikka[0]
                print ("Haluatko ostaa toisen paikan?")
                uusi_input = input("1 = kyllä, 2 = ei: ")
                if uusi_input == "1":
                    pass
                else:
                    g = False
        elif uusi_input == "3":
            print ("Kokonaishinta:", str(myynti) + "€")
        elif uusi_input == "2":
            xd = 0
            while xd < 6:
                print ("Rivi " + str(xd + 29) + ": on " + str((1 + (20 * xd)) + 29) + " €" )
                xd = xd + 1
        elif uusi_input == "1":
            nayta_paikat(varatut_paikat)
        else:
            print (pun + "Virheellinen vaihtoehto" + norm)

    print()
    print("Leffaherkut voit tilata ennakkoon")

print()
print("Mobililaitteet tulee olla suljettuna koko elokuvan ajan!")
print("Nauti leffasta!")


tiedosto = open("NeSi_naytokset.json", "w")
json.dump(näytökset, tiedosto, indent=2)
tiedosto.close()
